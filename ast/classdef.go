package ast

import (
	pegn "gitlab.com/pegn/libpegn"
	"gitlab.com/pegn/libpegn/ast/nd"
	"gitlab.com/pegn/libpegn/is"
)

// ClassDef <-- ClassId SP+ '<-' SP+ ClassExpr
func ClassDef(p pegn.Parser) (*pegn.Node, error) {

	node := pegn.NewNode(nd.ClassDef, nd.Types)

	var err error
	var n *pegn.Node
	beg := p.Mark()

	// ClassId
	n, err = ClassId(p)
	if err != nil {
		p.Goto(beg)
		return expected("ClassId", node, p)
	}
	node.AppendChild(n)

	// SP+ '<-' SP+
	if p.Expect(is.Min{' ', 1}, "<-", is.Min{' ', 1}) == nil {
		p.Goto(beg)
		return expected("SP+ '<-' SP+", node, p)
	}

	// ClassExpr
	n, err = ClassExpr(p)
	if err != nil {
		p.Goto(beg)
		return expected("ClassExpr", node, p)
	}
	node.AppendChild(n)

	return node, nil
}
