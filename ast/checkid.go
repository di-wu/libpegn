package ast

import (
	pegn "gitlab.com/pegn/libpegn"
	"gitlab.com/pegn/libpegn/ast/nd"
	"gitlab.com/pegn/libpegn/is"
)

// CheckId <-- ResCheckId / (upper lower+)+
func CheckId(p pegn.Parser) (*pegn.Node, error) {

	node := pegn.NewNode(nd.CheckId, nd.Types)

	var n *pegn.Node
	var m *pegn.Mark

	// ResCheckId
	n, err := ResCheckId(p)
	if err == nil {
		node.AppendChild(n)
		return node, nil
	}

	// (upper lower+)+
	m = p.Check(is.Min{is.Seq{is.Upper, is.Min{is.Lower, 1}}, 1})
	if m == nil {
		return expected("(upper lower+)+", node, p)
	}
	node.Value = p.Parse(m)
	p.Goto(m)
	p.Next()

	return node, nil
}
