package ast_test

import (
	pegn "gitlab.com/pegn/libpegn"
	"gitlab.com/pegn/libpegn/ast"
	"gitlab.com/pegn/libpegn/parser"
)

func ExampleSequence() {
	var n *pegn.Node
	p := parser.New()
	p.Init("Rule Another*")
	n, _ = ast.Sequence(p)
	n.Print()
	// Output:
	// ["Sequence", [
	//   ["Plain", [
	//     ["CheckId", "Rule"]
	//   ]],
	//   ["Plain", [
	//     ["CheckId", "Another"],
	//     ["MinZero", "*"]
	//   ]]
	// ]]
}
