package ast_test

import (
	pegn "gitlab.com/pegn/libpegn"
	"gitlab.com/pegn/libpegn/ast"
	"gitlab.com/pegn/libpegn/parser"
)

func ExampleRule() {

	var n *pegn.Node

	// Rule <-- Primary Quant?
	p := parser.New()

	p.Init("\"<--\"")
	n, _ = ast.Rule(p)
	n.Print()

	p.Init("IsCheckId")
	n, _ = ast.Rule(p)
	n.Print()

	p.Init("!any")
	n, _ = ast.Rule(p)
	n.Print()

	p.Init("Another{2}")
	n, _ = ast.Rule(p)
	n.Print()

	p.Init("Rule Another")
	n, _ = ast.Rule(p)
	n.Print()
	ast.Spacing(p)
	n, _ = ast.Rule(p)
	n.Print()

	// Output:
	// <nil>
	// ["Rule", [
	//   ["Plain", [
	//     ["CheckId", "IsCheckId"]
	//   ]]
	// ]]
	// ["Rule", [
	//   ["NegLook", [
	//     ["ClassId", [
	//       ["ResClassId", "any"]
	//     ]]
	//   ]]
	// ]]
	// ["Rule", [
	//   ["Plain", [
	//     ["CheckId", "Another"],
	//     ["Count", "2"]
	//   ]]
	// ]]
	// ["Rule", [
	//   ["Plain", [
	//     ["CheckId", "Rule"]
	//   ]]
	// ]]
	// ["Rule", [
	//   ["Plain", [
	//     ["CheckId", "Another"]
	//   ]]
	// ]]

}
