package ast_test

import (
	pegn "gitlab.com/pegn/libpegn"
	"gitlab.com/pegn/libpegn/ast"
	"gitlab.com/pegn/libpegn/parser"
)

func ExamplePrimary_token() {
	var n *pegn.Node
	p := parser.New()
	p.Init("TK")
	n, _ = ast.Primary(p)
	n.Print()
	// Output:
	// ["Primary", [
	//   ["TokenId", "TK"]
	// ]]
}

func ExamplePrimary_checkid() {
	var n *pegn.Node
	p := parser.New()
	p.Init("MyCheckId")
	n, _ = ast.Primary(p)
	n.Print()
	// Output:
	// ["Primary", [
	//   ["CheckId", "MyCheckId"]
	// ]]
}

func ExamplePrimary_expression() {
	var n *pegn.Node
	p := parser.New()
	p.Init("(Some Expr Here?)")
	n, _ = ast.Primary(p)
	n.Print()
	// Output:
	// ["Primary", [
	//   ["Expression", [
	//     ["Sequence", [
	//       ["Plain", [
	//         ["CheckId", "Some"]
	//       ]],
	//       ["Plain", [
	//         ["CheckId", "Expr"]
	//       ]],
	//       ["Plain", [
	//         ["CheckId", "Here"],
	//         ["Optional", "?"]
	//       ]]
	//     ]]
	//   ]]
	// ]]
}

func ExamplePrimary_string() {
	var n *pegn.Node
	p := parser.New()
	p.Init("<--")
	n, _ = ast.Primary(p)
	n.Print()
	// Output:
	// <nil>
}
