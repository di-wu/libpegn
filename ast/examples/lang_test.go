package ast_test

import (
	pegn "gitlab.com/pegn/libpegn"
	"gitlab.com/pegn/libpegn/ast"
	"gitlab.com/pegn/libpegn/parser"
)

func ExampleLang() {
	var n *pegn.Node

	// Lang <-- upper{2,12}
	p := parser.New()

	// PEGN
	p.Init("PEGN")
	n, _ = ast.Lang(p)
	n.Print()

	// Output:
	// ["Lang", "PEGN"]

}
