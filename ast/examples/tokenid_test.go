package ast_test

import (
	pegn "gitlab.com/pegn/libpegn"
	"gitlab.com/pegn/libpegn/ast"
	"gitlab.com/pegn/libpegn/parser"
)

func ExampleTokenId() {

	var n *pegn.Node

	// TokenId <-- ResTokenId / upper (upper / UNDER upper)+
	p := parser.New()

	// ResTokenId
	p.Init("DOT")
	n, _ = ast.TokenId(p)
	n.Print()

	// TOKENID
	p.Init("TOKENID")
	n, _ = ast.TokenId(p)
	n.Print()

	// TOKEN_ID
	p.Init("TOKEN_ID")
	n, _ = ast.TokenId(p)
	n.Print()

	// _TOKEN_ID
	p.Init("_TOKEN_ID")
	n, _ = ast.TokenId(p)
	n.Print()

	// TOKEN_ID_
	p.Init("TOKEN_ID_")
	n, _ = ast.TokenId(p)
	n.Print()

	p.Print()

	// Output:
	// ["TokenId", [
	//   ["ResTokenId", "DOT"]
	// ]]
	// ["TokenId", "TOKENID"]
	// ["TokenId", "TOKEN_ID"]
	// <nil>
	// ["TokenId", "TOKEN_ID"]
	// U+005F '_' 1,9-9 (9-9)

}
