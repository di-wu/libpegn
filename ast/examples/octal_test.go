package ast_test

import (
	pegn "gitlab.com/pegn/libpegn"
	"gitlab.com/pegn/libpegn/ast"
	"gitlab.com/pegn/libpegn/parser"
)

func ExampleOctal() {
	var n *pegn.Node

	// Octal <-- "o" octdig+
	p := parser.New()

	// o7
	p.Init("o7")
	n, _ = ast.Octal(p)
	n.Print()

	// o007
	p.Init("o007")
	n, _ = ast.Octal(p)
	n.Print()

	// Output:
	// ["Octal", "o7"]
	// ["Octal", "o007"]

}
