package ast_test

import (
	pegn "gitlab.com/pegn/libpegn"
	"gitlab.com/pegn/libpegn/ast"
	"gitlab.com/pegn/libpegn/parser"
)

func ExampleClassDef_ranges() {
	var n *pegn.Node
	p := parser.New()
	p.Init("alpha <- [A-Z] / [a-z]")
	n, _ = ast.ClassDef(p)
	n.Print()
	// Output:
	// ["ClassDef", [
	//   ["ClassId", [
	//     ["ResClassId", "alpha"]
	//   ]],
	//   ["ClassExpr", [
	//     ["AlphaRange", [
	//       ["Alpha", "A"],
	//       ["Alpha", "Z"]
	//     ]],
	//     ["AlphaRange", [
	//       ["Alpha", "a"],
	//       ["Alpha", "z"]
	//     ]]
	//   ]]
	// ]]
}

func ExampleClassDef_classes() {
	var n *pegn.Node
	p := parser.New()
	p.Init("alnum <- alphanum")
	n, _ = ast.ClassDef(p)
	n.Print()
	// Output:
	// ["ClassDef", [
	//   ["ClassId", [
	//     ["ResClassId", "alnum"]
	//   ]],
	//   ["ClassExpr", [
	//     ["ClassId", [
	//       ["ResClassId", "alphanum"]
	//     ]]
	//   ]]
	// ]]
}
