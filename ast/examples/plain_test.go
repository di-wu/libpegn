package ast_test

import (
	pegn "gitlab.com/pegn/libpegn"
	"gitlab.com/pegn/libpegn/ast"
	"gitlab.com/pegn/libpegn/parser"
)

func ExamplePlain() {
	var n *pegn.Node
	p := parser.New()

	p.Init("ThisIsPlain")
	n, _ = ast.Plain(p)
	n.Print()

	p.Init("class")
	n, _ = ast.Plain(p)
	n.Print()

	p.Init("TOKEN")
	n, _ = ast.Plain(p)
	n.Print()

	// Output:
	// ["Plain", [
	//   ["CheckId", "ThisIsPlain"]
	// ]]
	// ["Plain", [
	//   ["ClassId", "class"]
	// ]]
	// ["Plain", [
	//   ["TokenId", "TOKEN"]
	// ]]

}
