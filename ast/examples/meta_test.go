package ast_test

import (
	pegn "gitlab.com/pegn/libpegn"
	"gitlab.com/pegn/libpegn/ast"
	"gitlab.com/pegn/libpegn/parser"
)

func ExampleMeta() {
	var n *pegn.Node

	// Meta <- "# " Language " (" Version ") " Home EndLine
	p := parser.New()

	// # PEGN (v0.31.1) gitlab.com/pegn/spec
	p.Init("# PEGN (v0.31.1) gitlab.com/pegn/spec\n")
	n, _ = ast.Meta(p)
	n.Print()

	// Output:
	// ["Meta", [
	//   ["Lang", "PEGN"],
	//   ["MajorVer", "0"],
	//   ["MinorVer", "31"],
	//   ["PatchVer", "1"],
	//   ["Home", "gitlab.com/pegn/spec"],
	//   ["EndLine", "\n"]
	// ]]

}
