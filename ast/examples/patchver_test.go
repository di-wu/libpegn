package ast_test

import (
	pegn "gitlab.com/pegn/libpegn"
	"gitlab.com/pegn/libpegn/ast"
	"gitlab.com/pegn/libpegn/parser"
)

func ExamplePatchVer() {

	var n *pegn.Node

	// PatchVer <-- digit+
	p := parser.New()

	// 1
	p.Init("1")
	n, _ = ast.PatchVer(p)
	n.Print()

	// Output:
	// ["PatchVer", "1"]

}
