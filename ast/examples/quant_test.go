package ast_test

import (
	pegn "gitlab.com/pegn/libpegn"
	"gitlab.com/pegn/libpegn/ast"
	"gitlab.com/pegn/libpegn/parser"
)

func ExampleQuant() {
	var n *pegn.Node

	// Quant  <- Optional / MinZero / MinOne / MinMax / Count
	p := parser.New()

	// ?
	p.Init("?")
	n, _ = ast.Quant(p)
	n.Print()

	// +
	p.Init("+")
	n, _ = ast.Quant(p)
	n.Print()

	// *
	p.Init("*")
	n, _ = ast.Quant(p)
	n.Print()

	// {1,3}
	p.Init("{1,3}")
	n, _ = ast.Quant(p)
	n.Print()

	// {5}
	p.Init("{5}")
	n, _ = ast.Quant(p)
	n.Print()

	// Output:
	// ["Quant", [
	//   ["Optional", "?"]
	// ]]
	// ["Quant", [
	//   ["MinOne", "+"]
	// ]]
	// ["Quant", [
	//   ["MinZero", "*"]
	// ]]
	// ["Quant", [
	//   ["MinMax", [
	//     ["Min", "1"],
	//     ["Max", "3"]
	//   ]]
	// ]]
	// ["Quant", [
	//   ["Count", "5"]
	// ]]

}
