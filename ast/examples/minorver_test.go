package ast_test

import (
	pegn "gitlab.com/pegn/libpegn"
	"gitlab.com/pegn/libpegn/ast"
	"gitlab.com/pegn/libpegn/parser"
)

func ExampleMinorVer() {
	var n *pegn.Node

	// MinorVer <-- digit+
	p := parser.New()

	// 31
	p.Init("31")
	n, _ = ast.MinorVer(p)
	n.Print()

	// Output:
	// ["MinorVer", "31"]

}
