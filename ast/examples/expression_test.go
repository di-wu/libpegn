package ast_test

import (
	pegn "gitlab.com/pegn/libpegn"
	"gitlab.com/pegn/libpegn/ast"
	"gitlab.com/pegn/libpegn/parser"
)

func ExampleExpression_multiple() {
	var n *pegn.Node
	p := parser.New()
	p.Init("Rule / Another")
	n, _ = ast.Expression(p)
	n.Print()
	// Output:
	// ["Expression", [
	//   ["Sequence", [
	//     ["Plain", [
	//       ["CheckId", "Rule"]
	//     ]]
	//   ]],
	//   ["Sequence", [
	//     ["Plain", [
	//       ["CheckId", "Another"]
	//     ]]
	//   ]]
	// ]]
}

func ExampleExpression_seq() {
	var n *pegn.Node
	p := parser.New()
	p.Init("Here &'are' rules*")
	n, _ = ast.Expression(p)
	n.Print()
	// Output:
	// ["Expression", [
	//   ["Sequence", [
	//     ["Plain", [
	//       ["CheckId", "Here"]
	//     ]],
	//     ["PosLook", [
	//       ["String", "are"]
	//     ]],
	//     ["Plain", [
	//       ["ClassId", "rules"],
	//       ["MinZero", "*"]
	//     ]]
	//   ]]
	// ]]
}
