package ast_test

import (
	pegn "gitlab.com/pegn/libpegn"
	"gitlab.com/pegn/libpegn/ast"
	"gitlab.com/pegn/libpegn/parser"
)

func ExampleMinOne() {
	var n *pegn.Node

	// MinOne <-- "+"
	p := parser.New()

	// +
	p.Init("+")
	n, _ = ast.MinOne(p)
	n.Print()

	// Output:
	// ["MinOne", "+"]

}
