package ast_test

import (
	pegn "gitlab.com/pegn/libpegn"
	"gitlab.com/pegn/libpegn/ast"
	"gitlab.com/pegn/libpegn/parser"
)

func ExampleNegLook_plain() {
	var n *pegn.Node
	p := parser.New()
	p.Init("!MyCheck")
	n, _ = ast.NegLook(p)
	n.Print()
	// Output:
	// ["NegLook", [
	//   ["CheckId", "MyCheck"]
	// ]]
}

func ExampleNegLook_minzero() {
	var n *pegn.Node
	p := parser.New()
	p.Init("!MyZeroOrMore*")
	n, _ = ast.NegLook(p)
	n.Print()
	// Output:
	// ["NegLook", [
	//   ["CheckId", "MyZeroOrMore"],
	//   ["MinZero", "*"]
	// ]]
}

func ExampleNegLook_minone() {
	var n *pegn.Node
	p := parser.New()
	p.Init("!MyOneOrMore+")
	n, _ = ast.NegLook(p)
	n.Print()
	// Output:
	// ["NegLook", [
	//   ["CheckId", "MyOneOrMore"],
	//   ["MinOne", "+"]
	// ]]
}

func ExampleNegLook_minmax() {
	var n *pegn.Node
	p := parser.New()
	p.Init("!MyMinMax{2,4}")
	n, _ = ast.NegLook(p)
	n.Print()
	// Output:
	// ["NegLook", [
	//   ["CheckId", "MyMinMax"],
	//   ["MinMax", [
	//     ["Min", "2"],
	//     ["Max", "4"]
	//   ]]
	// ]]
}
