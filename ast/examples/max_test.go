package ast_test

import (
	pegn "gitlab.com/pegn/libpegn"
	"gitlab.com/pegn/libpegn/ast"
	"gitlab.com/pegn/libpegn/parser"
)

func ExampleMax() {
	var n *pegn.Node

	// Max <-- digit+
	p := parser.New()

	// 1
	p.Init("1")
	n, _ = ast.Max(p)
	n.Print()

	// 99
	p.Init("99")
	n, _ = ast.Max(p)
	n.Print()

	// Output:
	// ["Max", "1"]
	// ["Max", "99"]

}
