package ast_test

import (
	pegn "gitlab.com/pegn/libpegn"
	"gitlab.com/pegn/libpegn/ast"
	"gitlab.com/pegn/libpegn/parser"
)

func ExampleString() {

	var n *pegn.Node

	// String <-- quotable+
	p := parser.New()

	// ! a
	p.Init("! a")
	n, _ = ast.String(p)
	n.Print()

	// !
	p.Init("!")
	n, _ = ast.String(p)
	n.Print()

	// <--
	p.Init("<--")
	n, _ = ast.String(p)
	n.Print()

	// string
	p.Init("'string'")
	n, _ = ast.String(p)
	n.Print()

	// Output:
	// ["String", "! a"]
	// ["String", "!"]
	// ["String", "<--"]
	// <nil>

}
