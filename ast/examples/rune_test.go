package ast_test

import (
	pegn "gitlab.com/pegn/libpegn"
	"gitlab.com/pegn/libpegn/ast"
	"gitlab.com/pegn/libpegn/parser"
)

func ExampleRune() {

	var n *pegn.Node

	// Rune <- Unicode / Binary / Hexadec / Octal
	p := parser.New()

	p.Init("\"ABC\"")
	n, _ = ast.Rune(p)
	n.Print()

	p.Init("U+000F")
	n, _ = ast.Rune(p)
	n.Print()

	p.Init("b10")
	n, _ = ast.Rune(p)
	n.Print()

	p.Init("x20")
	n, _ = ast.Hexadec(p)
	n.Print()

	p.Init("o744")
	n, _ = ast.Octal(p)
	n.Print()

	// Output:
	// <nil>
	// ["Rune", [
	//   ["Unicode", "U+000F"]
	// ]]
	// ["Rune", [
	//   ["Binary", "b10"]
	// ]]
	// ["Hexadec", "x20"]
	// ["Octal", "o744"]

}
