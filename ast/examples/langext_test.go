package ast_test

import (
	pegn "gitlab.com/pegn/libpegn"
	"gitlab.com/pegn/libpegn/ast"
	"gitlab.com/pegn/libpegn/parser"
)

func ExampleLangExt() {
	var n *pegn.Node

	// LangExt <-- visible{1,20}
	p := parser.New()

	// alpha
	p.Init("alpha")
	n, _ = ast.LangExt(p)
	n.Print()

	// Output:
	// ["LangExt", "alpha"]

}
