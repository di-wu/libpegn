package ast_test

import (
	pegn "gitlab.com/pegn/libpegn"
	"gitlab.com/pegn/libpegn/ast"
	"gitlab.com/pegn/libpegn/parser"
)

func ExampleDigit() {
	var n *pegn.Node

	// Digit <-- digit
	p := parser.New()

	// 0
	p.Init("0")
	n, _ = ast.Digit(p)
	n.Print()

	// 9
	p.Init("9")
	n, _ = ast.Digit(p)
	n.Print()

	// Output:
	// ["Digit", "0"]
	// ["Digit", "9"]

}
