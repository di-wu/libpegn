package ast_test

import (
	pegn "gitlab.com/pegn/libpegn"
	"gitlab.com/pegn/libpegn/ast"
	"gitlab.com/pegn/libpegn/parser"
)

func ExampleHome() {

	var n *pegn.Node

	// Home <-- (!ws any)+
	p := parser.New()

	// gitlab.com/pegn/spec
	p.Init("gitlab.com/pegn/spec")
	n, _ = ast.Home(p)
	n.Print()

	// Output:
	// ["Home", "gitlab.com/pegn/spec"]

}
