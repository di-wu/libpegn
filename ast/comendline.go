package ast

import (
	pegn "gitlab.com/pegn/libpegn"
	"gitlab.com/pegn/libpegn/ast/nd"
	"gitlab.com/pegn/libpegn/is"
)

// ComEndLine <- SP* ('# ' Comment)? EndLine
func ComEndLine(p pegn.Parser) (*pegn.Node, error) {

	node := pegn.NewNode(nd.ComEndLine, nd.Types)
	var n *pegn.Node
	var err error
	beg := p.Mark()

	// SP*
	p.Expect(is.Min{' ', 1})

	// ('# ' Comment)?
	for {

		b := p.Mark()

		// '# '
		if p.Expect("# ") == nil {
			p.Goto(b)
			break
		}

		// Comment
		n, err = Comment(p)
		if err != nil {
			p.Goto(b)
			break
		}
		node.AppendChild(n)

		break
	}

	// EndLine
	n, err = EndLine(p)
	if err != nil {
		p.Goto(beg)
		return expected("EndLine", node, p)
	}
	node.AppendChild(n)

	return node, nil
}
