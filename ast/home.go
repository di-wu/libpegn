package ast

import (
	pegn "gitlab.com/pegn/libpegn"
	"gitlab.com/pegn/libpegn/ast/nd"
	"gitlab.com/pegn/libpegn/is"
)

// Home <-- (!ws any)+
func Home(p pegn.Parser) (*pegn.Node, error) {

	node := pegn.NewNode(nd.Home, nd.Types)

	var m *pegn.Mark

	// (!ws any)+
	m = p.Check(is.Min{is.Seq{is.Not{is.WhiteSpace}, is.Any}, 1})
	if m == nil {
		return expected("(!ws any)+", node, p)
	}
	node.Value = p.Parse(m)
	p.Goto(m)
	p.Next()

	return node, nil
}
