package ast

import (
	pegn "gitlab.com/pegn/libpegn"
	"gitlab.com/pegn/libpegn/ast/nd"
)

// UniRange <-- '[' Uni '-' Uni ']'
func UniRange(p pegn.Parser) (*pegn.Node, error) {

	node := pegn.NewNode(nd.UniRange, nd.Types)

	var err error
	var n *pegn.Node
	beg := p.Mark()

	// '['
	if p.Expect("[") == nil {
		p.Goto(beg)
		return expected("'['", node, p)
	}

	// Uni
	n, err = Unicode(p)
	if err != nil {
		p.Goto(beg)
		return expected("Uni", node, p)
	}
	node.AppendChild(n)

	// '-'
	if p.Expect("-") == nil {
		p.Goto(beg)
		return expected("'-'", node, p)
	}

	// Uni
	n, err = Unicode(p)
	if err != nil {
		p.Goto(beg)
		return expected("Uni", node, p)
	}
	node.AppendChild(n)

	// ']'
	if p.Expect("]") == nil {
		p.Goto(beg)
		return expected("']'", node, p)
	}

	return node, nil
}
