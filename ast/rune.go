package ast

import (
	pegn "gitlab.com/pegn/libpegn"
	"gitlab.com/pegn/libpegn/ast/nd"
)

// Rune <- Unicode / Binary / Hexadec / Octal
func Rune(p pegn.Parser) (*pegn.Node, error) {

	node := pegn.NewNode(nd.Rune, nd.Types)

	var err error
	var n *pegn.Node

	// Unicode
	n, err = Unicode(p)
	if err == nil {
		node.AppendChild(n)
		return node, nil
	}

	// Binary
	n, err = Binary(p)
	if err == nil {
		node.AppendChild(n)
		return node, nil
	}

	// Hexadec
	n, err = Hexadec(p)
	if err == nil {
		node.AppendChild(n)
		return node, nil
	}

	// Octal
	n, err = Octal(p)
	if err == nil {
		node.AppendChild(n)
		return node, nil
	}

	return expected("Rune <- Unicode / Binary / Hexadec / Octal", node, p)
}
