package ast

import (
	pegn "gitlab.com/pegn/libpegn"
	"gitlab.com/pegn/libpegn/ast/nd"
)

// OctRange <-- '[' Octal '-' Octal ']'
func OctRange(p pegn.Parser) (*pegn.Node, error) {

	node := pegn.NewNode(nd.OctRange, nd.Types)

	var err error
	var n *pegn.Node
	beg := p.Mark()

	// '['
	if p.Expect('[') == nil {
		p.Goto(beg)
		return expected("'['", node, p)
	}

	// Octal
	n, err = Octal(p)
	if err != nil {
		p.Goto(beg)
		return expected("Octal", node, p)
	}
	node.AppendChild(n)

	// '-'
	if p.Expect('-') == nil {
		p.Goto(beg)
		return expected("'-'", node, p)
	}

	// Octal
	n, err = Octal(p)
	if err != nil {
		p.Goto(beg)
		return expected("Octal", node, p)
	}
	node.AppendChild(n)

	// ']'
	if p.Expect(']') == nil {
		p.Goto(beg)
		return expected("']'", node, p)
	}

	return node, nil
}
