package ast

import (
	pegn "gitlab.com/pegn/libpegn"
	"gitlab.com/pegn/libpegn/ast/nd"
)

// ResTokenId <-- 'TAB' / 'CRLF' / 'CR' / 'LF' / 'SP' / 'VT' / 'FF'
//              / 'NOT' / 'BANG' / 'DQ'
//              / 'HASH' / 'DOLLAR' / 'PERCENT' / 'AND' / 'SQ'
//              / 'LPAREN' / 'RPAREN' / 'STAR' / 'PLUS' / 'COMMA'
//              / 'DASH' / 'MINUS' / 'DOT' / 'SLASH' / 'COLON' / 'SEMI'
//              / 'LT' / 'EQ' / 'GT' / 'QUERY' / 'QUESTION' / 'AT'
//              / 'LBRAKT' / 'BKSLASH' / 'RBRAKT' / 'CARET' / 'UNDER'
//              / 'BKTICK' / 'LCURLY' / 'LBRACE' / 'BAR' / 'PIPE'
//              / 'RCULRY' / 'RBRACE' / 'TILDE' / 'UNKNOWN' / 'REPLACE'
//              / 'MAXRUNE' / 'MAXASCII' / 'MAXLATIN' / 'LARROW'
//              / 'RARROW' / 'LLARROW' / 'RLARROW' / 'LARROWF'
//              / 'LFAT' / 'RARROWF' / 'RFAT' / 'WALRUS'
func ResTokenId(p pegn.Parser) (*pegn.Node, error) {

	node := pegn.NewNode(nd.ResTokenId, nd.Types)
	var m *pegn.Mark

	for _, v := range []string{
		"TAB", "CRLF", "CR", "LF", "SP", "VT", "FF", "NOT", "BANG", "DQ",
		"HASH", "DOLLAR", "PERCENT", "AND", "SQ", "LPAREN",
		"RPAREN", "STAR", "PLUS", "COMMA", "DASH", "MINUS", "DOT",
		"SLASH", "COLON", "SEMI", "LT", "EQ", "GT", "QUERY",
		"QUESTION", "AT", "LBRAKT", "BKSLASH", "RBRAKT", "CARET",
		"UNDER", "BKTICK", "LCURLY", "LBRACE", "BAR", "PIPE",
		"RCULRY", "RBRACE", "TILDE", "UNKNOWN", "REPLACE", "MAXRUNE",
		"MAXASCII", "MAXLATIN", "LARROW", "RARROW", "LLARROW", "RLARROW",
		"LARROWF", "LFAT", "RARROWF", "RFAT", "WALRUS",
	} {
		m = p.Check(v)
		if m == nil {
			continue
		}
		node.Value += p.Parse(m)
		p.Goto(m)
		p.Next()

		return node, nil
	}

	return expected("'TAB'/'CRLF'/'CR'/'LF'/'SP'/'VT'/'FF'/'NOT'/'BANG'/'DQ'/'HASH'/'DOLLAR'/'PERCENT'/'AND'/'SQ'/'LPAREN'/'RPAREN'/'STAR'/'PLUS'/'COMMA'/'DASH'/'MINUS'/'DOT'/'SLASH'/'COLON'/'SEMI'/'LT'/'EQ'/'GT'/'QUERY'/'QUESTION'/'AT'/'LBRAKT'/'BKSLASH'/'RBRAKT'/'CARET'/'UNDER'/'BKTICK'/'LCURLY'/'LBRACE'/'BAR'/'PIPE'/'RCULRY'/'RBRACE'/'TILDE'/'UNKNOWN'/'REPLACE'/'MAXRUNE'/'MAXASCII'/'MAXLATIN'/'LARROW'/'RARROW'/'LLARROW'/'RLARROW'/'LARROWF'/'LFAT'/'RARROWF'/'RFAT'/'WALRUS'", node, p)
}
