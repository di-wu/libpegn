package ast

import (
	pegn "gitlab.com/pegn/libpegn"
	"gitlab.com/pegn/libpegn/ast/nd"
	"gitlab.com/pegn/libpegn/is"
)

// String <- quotable+
func String(p pegn.Parser) (*pegn.Node, error) {

	node := pegn.NewNode(nd.String, nd.Types)

	var m *pegn.Mark

	// quotable+
	m = p.Check(is.Min{is.Quotable, 1})
	if m == nil {
		return expected("quotable+", node, p)
	}
	node.Value += p.Parse(m)
	p.Goto(m)
	p.Next()

	return node, nil
}
