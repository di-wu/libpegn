package ast

import (
	pegn "gitlab.com/pegn/libpegn"
	"gitlab.com/pegn/libpegn/ast/nd"
)

// Language <- Lang ('-' LangExt)?
func Language(p pegn.Parser) (*pegn.Node, error) {

	node := pegn.NewNode(nd.Language, nd.Types)

	var err error
	var n *pegn.Node

	// Lang
	n, err = Lang(p)
	if err != nil {
		return expected("Lang", node, p)
	}
	node.AppendChild(n)

	for {

		// '-'
		if p.Expect('-') == nil {
			break
		}

		// LangExt?
		n, err = LangExt(p)
		if err != nil {
			break
		}
		node.AppendChild(n)

		break
	}

	return node, nil
}
