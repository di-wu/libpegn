// Copyright 2020 Rob Muhlestein.
// Use of this source code is governed by the Apache
// 2.0 license that can be found in the LICENSE file.

package pegn

import (
	"encoding/json"
	"fmt"
	"strings"
)

// Action is a first-class function used when Visiting each Node. The
// return value will be sent to a channel as each Action completes. It
// can be an error or something else.
type Action func(n *Node) interface{}

type Node struct {
	Type       int // 0 = nd.Undefined
	Types      []string
	Value      string // only for literal types (no children)
	Parent     *Node
	PrevSib    *Node
	NextSib    *Node
	FirstChild *Node
	LastChild  *Node
}

func NewNode(typ int, types []string) *Node {
	n := new(Node)
	n.Type = typ
	n.Types = types
	return n
}

func (n *Node) TypeName() string { return n.Types[n.Type] }

func (n *Node) InsertBeforeSelf(c *Node) {
	c.Parent = n.Parent
	if n.PrevSib == nil {
		c.NextSib = n
		n.PrevSib = c
		if n.Parent != nil {
			n.Parent.FirstChild = c
		}
		return
	}
	c.PrevSib = n.PrevSib
	c.NextSib = n
	n.PrevSib.NextSib = c
	n.PrevSib = c
}

func (n *Node) AppendAfterSelf(c *Node) {
	c.Parent = n.Parent
	if n.NextSib == nil {
		c.PrevSib = n
		n.NextSib = c
		if n.Parent != nil {
			n.Parent.LastChild = c
		}
		return
	}
	c.NextSib = n.NextSib
	c.PrevSib = n
	n.NextSib.PrevSib = c
	n.NextSib = c
}

func (n *Node) RemoveSelf() *Node {
	if n.Parent != nil {
		if n.Parent.FirstChild == n {
			n.Parent.FirstChild = n.NextSib
		}
		if n.Parent.LastChild == n {
			n.Parent.LastChild = n.PrevSib
		}
	}
	if n.PrevSib != nil {
		n.PrevSib.NextSib = n.NextSib
	}
	if n.NextSib != nil {
		n.NextSib.PrevSib = n.PrevSib
	}
	n.Parent = nil
	n.NextSib = nil
	n.PrevSib = nil
	return n
}

func (n *Node) ReplaceSelf(c *Node) *Node {
	c.Parent = n.Parent
	c.PrevSib = n.PrevSib
	c.NextSib = n.NextSib
	if n.Parent.LastChild == n {
		n.Parent.LastChild = c
	}
	if n.Parent.FirstChild == n {
		n.Parent.FirstChild = c
	}
	if n.PrevSib != nil {
		n.PrevSib.NextSib = c
	}
	if n.NextSib != nil {
		n.NextSib.PrevSib = c
	}
	n.Parent = nil
	n.NextSib = nil
	n.PrevSib = nil
	return n
}

func (n *Node) Children() []*Node {
	if n.FirstChild == nil {
		return nil
	}
	cur := n.FirstChild
	c := []*Node{cur}
	for {
		cur = cur.NextSib
		if cur == nil {
			break
		}
		c = append(c, cur)
	}
	return c
}

func (n *Node) AppendChild(c *Node) {
	if n.FirstChild == nil {
		c.Parent = n
		n.FirstChild = c
		n.LastChild = c
		return
	}
	n.LastChild.AppendAfterSelf(c)
}

func (n *Node) AdoptFrom(other *Node) {
	if other.FirstChild == nil {
		return
	}
	c := other.FirstChild.RemoveSelf()
	n.AppendChild(c)
	n.AdoptFrom(other)
}

func (n *Node) Visit(act Action, rvals chan interface{}) {
	if rvals == nil {
		act(n)
	} else {
		rvals <- act(n)
	}
	if n.FirstChild == nil {
		return
	}
	for _, c := range n.Children() {
		c.Visit(act, rvals)
	}
	return
}

// VisitAsync walks the Node and all its Children asynchronously by
// flattening the Node tree into a one-dimensional array and then
// sending each Node to its own goroutine Action call. The limit must
// sets the maximum number of simultaneous goroutines (which can safely
// be in the thousands usually) and must be 2 or more or will panic. The
// return values channel will be sent all return values if defined.
// Otherwise it is ignored.
func (n *Node) VisitAsync(act Action, lim int, rvals chan interface{}) {
	nodes := []*Node{}

	if lim < 2 {
		panic("visitasync: limit must be 2 or more")
	}

	add := func(node *Node) interface{} {
		nodes = append(nodes, node)
		return nil
	}

	n.Visit(add, nil)

	// use buffered channel to throttle
	sem := make(chan interface{}, lim)
	for _, node := range nodes {
		sem <- true
		if rvals == nil {
			go func(node *Node) {
				defer func() { <-sem }()
				act(node)
			}(node)
			continue
		} else {
			go func(node *Node) {
				defer func() { <-sem }()
				rvals <- act(node)
			}(node)
		}
	}

	// waits for all (keeps filling until full again)
	for i := 0; i < cap(sem); i++ {
		sem <- true
	}

	// all goroutines have now finished
	if rvals != nil {
		close(rvals)
	}

}

// MarshalJSON fulfills the interface and avoids use of slower
// reflection-based parsing. Nodes must be either containers ([1,[]]) or
// literals ([1,"foo"]). Only a two-element array is needed. JSON is the
// primary method for combining ASTs and therefore high priority has
// been given to the most compact JSON possible.  For better readability
// and more verbose test cases use PrettyString() and PrettyPrint()
// instead.
func (n *Node) MarshalJSON() ([]byte, error) {
	ch := n.Children()
	if len(ch) == 0 {
		if n.Value == "" {
			if n.Type == 0 {
				return []byte("[]"), nil
			}
			return []byte(fmt.Sprintf(`[%d]`, n.Type)), nil
		}
		return []byte(fmt.Sprintf(`[%d,"%v"]`, n.Type, JSONString(n.Value))), nil
	}
	byt, _ := ch[0].MarshalJSON()
	chs := "[" + string(byt)
	for _, child := range ch[1:] {
		byt, _ = child.MarshalJSON()
		chs += "," + string(byt)
	}
	chs += "]"
	return []byte(fmt.Sprintf(`[%d,%v]`, n.Type, chs)), nil
}

func (n *Node) UnmarshalJSON(b []byte) error {
	u := []interface{}{}
	err := json.Unmarshal(b, &u) // TODO drop dep on json package
	if err != nil {
		return err
	}
	// empty array ([]) is simplest node type, assume nd.Unknown
	if len(u) == 0 {
		return nil
	}
	// everything has a type integer as first item in list
	n.Type = int(u[0].(float64))
	if len(u) == 1 { // [2]
		return nil
	}
	// nodes are either containers or literals, never both
	switch v := u[1].(type) {
	case string: // [2,"some"]
		n.Value = v
	case [][]byte: // [2, [[3],[],[2,"some"],[4,[[3]]]]]
		n.FirstChild = new(Node)
		err = n.FirstChild.UnmarshalJSON(v[0])
		if err != nil {
			return err
		}
		cur := n.FirstChild
		for _, m := range v[1:] {
			c := new(Node)
			err := c.UnmarshalJSON(m)
			if err != nil {
				return nil
			}
			cur.AppendAfterSelf(c)
			cur = c
		}
		n.LastChild = cur
	}
	return nil
}

// JSON returns the compact output of MarshalJSON() suitable for AST
// serialization, exchange, and composition. Use this when storing
// and sharing. It is the only supported unmashaling format. Use
// String() or Print() for testing, debugging, and documentation.
func (n *Node) JSON() string {
	byt, _ := n.MarshalJSON()
	return string(byt)
}

// String fulfills the Stringer interface by checking for Types and
// if found pretty-printing with the Node.Type integers replaced
// with strings. Otherwise, returns the terse JSON string
// instead.
func (n *Node) String() string {
	if n.Types != nil {
		return n.pretty(0)
	}
	return n.JSON()
}

func (n *Node) Print() {
	if n == nil {
		fmt.Println("<nil>")
		return
	}
	fmt.Println(n.String())
}

func (n *Node) pretty(depth int) (jsn string) {
	indent := strings.Repeat(" ", depth*2)
	depth++
	jsn += fmt.Sprintf(`%v["%v", `, indent, n.Types[n.Type])
	if n.FirstChild != nil {
		jsn += "[\n"
		kinder := n.Children()
		for i, c := range kinder {
			jsn += c.pretty(depth)
			if i != len(kinder)-1 {
				jsn += ",\n"
			} else {
				jsn += fmt.Sprintf("\n%v]", indent)
			}
		}
		jsn += "]"
	} else {
		jsn += fmt.Sprintf(`"%v"]`, JSONString(n.Value))
	}
	return
}
