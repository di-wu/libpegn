package tk_test

import (
	"fmt"

	"gitlab.com/pegn/go/tk"
)

func Example_runes() {
	set := []rune{
		tk.TAB, tk.LF, tk.CR, tk.SP, tk.NOT, tk.BANG, tk.DQ,
		tk.HASH, tk.DOLLAR, tk.PERCENT, tk.AND, tk.SQ, tk.LPAREN,
		tk.RPAREN, tk.STAR, tk.PLUS, tk.COMMA, tk.DASH, tk.DOT,
		tk.SLASH, tk.COLON, tk.SEMI, tk.LT, tk.EQ, tk.GT, tk.QUERY,
		tk.QUESTION, tk.AT, tk.LBRAKT, tk.BKSLASH, tk.RBRAKT,
		tk.LCURLY, tk.LBRACE, tk.BAR, tk.PIPE, tk.RCURLY, tk.RBRACE,
		tk.TILDE, tk.UNKNOWN,
	}

	for _, c := range set {
		fmt.Printf("%q ", c)
	}

	// Output:
	// '\t' '\n' '\r' ' ' '!' '!' '"' '#' '$' '%' '&' '\'' '(' ')' '*' '+' ',' '-' '.' '/' ':' ';' '<' '=' '>' '?' '?' '@' '[' '\\' ']' '{' '{' '|' '|' '}' '}' '~' '�'

}

func Example_strings() {
	set := []string{
		tk.LARROW, tk.RARROW, tk.LFATARR, tk.RFATARR, tk.WALRUS,
	}

	for _, c := range set {
		fmt.Printf("%q ", c)
	}

	// Output:
	// "<-" "->" "<=" "=>" ":="
}
