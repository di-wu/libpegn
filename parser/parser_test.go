package parser

/*
func TestParser_bufferInput(t *testing.T) {
	p := new(parser)
	var e error
	// byte array is used directly
	e = p.bufferInput([]byte("something"))
	require.Equal(t, []byte("something"), p.buf)
	require.Nil(t, e)
	// string cast to byte array
	e = p.bufferInput("something")
	require.Equal(t, []byte("something"), p.buf)
	require.Nil(t, e)
	// io.Reader fully loaded
	e = p.bufferInput(strings.NewReader("something"))
	require.Equal(t, []byte("something"), p.buf)
	require.Nil(t, e)
	// single rune fails but does not affect current buffer
	e = p.bufferInput('d')
	require.NotNil(t, e)
	require.Equal(t, []byte("something"), p.buf)
	// integer fails but does not affect current buffer
	e = p.bufferInput(34)
	require.NotNil(t, e)
	require.Equal(t, []byte("something"), p.buf)
}

func TestParser_Init(t *testing.T) {
	p := new(parser)
	require.Nil(t, p.mk)
	p.Init("something")
	require.NotNil(t, p.mk)
	// should have read the first rune of first line
	require.Equal(t, 's', p.mk.Rune)
	require.Equal(t, 0, p.mk.Byte)
	require.Equal(t, 1, p.mk.Len)
	require.Equal(t, 1, p.mk.Next)
	require.Equal(t, 1, p.mk.Pos.Line)
	require.Equal(t, 1, p.mk.Pos.LineRune)
	require.Equal(t, 1, p.mk.Pos.LineByte)
	require.Equal(t, p.Mark(), p.mk)
}

func TestParser_Next(t *testing.T) {
	p := new(parser)
	p.Init("Hi 👌 u.")

	require.Equal(t, 'H', p.mk.Rune)
	require.Equal(t, 0, p.mk.Byte)
	require.Equal(t, 1, p.mk.Len)
	require.Equal(t, 1, p.mk.Next)
	require.Equal(t, 1, p.mk.Pos.Line)
	require.Equal(t, 1, p.mk.Pos.LineRune)
	require.Equal(t, 1, p.mk.Pos.LineByte)
	p.Next()

	require.Equal(t, 'i', p.mk.Rune)
	require.Equal(t, 1, p.mk.Byte)
	require.Equal(t, 1, p.mk.Len)
	require.Equal(t, 2, p.mk.Next)
	require.Equal(t, 1, p.mk.Pos.Line)
	require.Equal(t, 2, p.mk.Pos.LineRune)
	require.Equal(t, 2, p.mk.Pos.LineByte)
	p.Next()

	require.Equal(t, ' ', p.mk.Rune)
	require.Equal(t, 2, p.mk.Byte)
	require.Equal(t, 1, p.mk.Len)
	require.Equal(t, 3, p.mk.Next)
	require.Equal(t, 1, p.mk.Pos.Line)
	require.Equal(t, 3, p.mk.Pos.LineRune)
	require.Equal(t, 3, p.mk.Pos.LineByte)
	p.Next()

	require.Equal(t, '👌', p.mk.Rune)
	require.Equal(t, 3, p.mk.Byte)
	require.Equal(t, 4, p.mk.Len)
	require.Equal(t, 7, p.mk.Next)
	require.Equal(t, 1, p.mk.Pos.Line)
	require.Equal(t, 4, p.mk.Pos.LineRune)
	require.Equal(t, 4, p.mk.Pos.LineByte)
	p.Next()

	require.Equal(t, ' ', p.mk.Rune)
	require.Equal(t, 7, p.mk.Byte)
	require.Equal(t, 1, p.mk.Len)
	require.Equal(t, 8, p.mk.Next)
	require.Equal(t, 1, p.mk.Pos.Line)
	require.Equal(t, 5, p.mk.Pos.LineRune)
	require.Equal(t, 8, p.mk.Pos.LineByte)
	p.Next()

	require.Equal(t, 'u', p.mk.Rune)
	require.Equal(t, 8, p.mk.Byte)
	require.Equal(t, 1, p.mk.Len)
	require.Equal(t, 9, p.mk.Next)
	require.Equal(t, 1, p.mk.Pos.Line)
	require.Equal(t, 6, p.mk.Pos.LineRune)
	require.Equal(t, 9, p.mk.Pos.LineByte)
	p.Next()

	require.Equal(t, '.', p.mk.Rune)
	require.Equal(t, 9, p.mk.Byte)
	require.Equal(t, 1, p.mk.Len)
	require.Equal(t, 10, p.mk.Next)
	require.Equal(t, 1, p.mk.Pos.Line)
	require.Equal(t, 7, p.mk.Pos.LineRune)
	require.Equal(t, 10, p.mk.Pos.LineByte)
	p.Next()

	require.Equal(t, '�', p.mk.Rune)
	require.Equal(t, 9, p.mk.Byte)
	require.Equal(t, 0, p.mk.Len) // we're done
	require.Equal(t, 10, p.mk.Next)
	require.Equal(t, 1, p.mk.Pos.Line)
	require.Equal(t, 8, p.mk.Pos.LineRune)
	require.Equal(t, 10, p.mk.Pos.LineByte)
	p.Next()

}
*/
